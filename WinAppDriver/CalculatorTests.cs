﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace WinAppDriver
{
    [TestClass]
    public class CalculatorTests : CalculatorPageObject
    {
        [ClassInitialize]
        public static void ClassInitialize(TestContext context)
        {
            Setup(context);
            SetStandardMode();
        }

        [ClassCleanup]
        public static void ClassCleanup()
        {
            Close();
        }

        [TestInitialize]
        public void TestCleanup()
        {
            ClearResults();
        }

        [TestMethod]
        public void Addition()
        {
            OneButton.Click();
            PlusButton.Click();
            TwoButton.Click();
            EqualsButton.Click();

            Assert.AreEqual("3", GetCalculatorResultText(), "The result is wrong.");
        }

        [TestMethod]
        public void Subtraction()
        {
            SevenButton.Click();
            MinusButton.Click();
            TwoButton.Click();
            EqualsButton.Click();

            Assert.AreEqual("5", GetCalculatorResultText(), "The result is wrong.");
        }

        [TestMethod]
        public void Multiplication()
        {
            ThreeButton.Click();
            MultiplyButton.Click();
            NineButton.Click();
            EqualsButton.Click();

            Assert.AreEqual("27", GetCalculatorResultText(), "The result is wrong.");
        }

        [TestMethod]
        public void Division()
        {
            EightButton.Click();
            EightButton.Click();
            DivideButton.Click();
            TwoButton.Click();
            EqualsButton.Click();

            Assert.AreEqual("44", GetCalculatorResultText());
        }
    }
}
