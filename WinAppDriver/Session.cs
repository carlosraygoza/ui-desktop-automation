﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Windows;
using System;
using System.Diagnostics;

namespace WinAppDriver
{
    public class Session
    {
        private const string WindowsApplicationDriverUrl = "http://127.0.0.1:4723";
        protected static WindowsDriver<WindowsElement> session;
        protected static Process WinAppDriverProcess;
        private const string CalculatorAppId = "Microsoft.WindowsCalculator_8wekyb3d8bbwe!App";

        public static void Setup(TestContext context)
        {
            WinAppDriverProcess = Process.Start(@"C:\Program Files (x86)\Windows Application Driver\WinAppDriver.exe");

            var opt = new AppiumOptions();
            opt.AddAdditionalCapability("app", CalculatorAppId);
            session = new WindowsDriver<WindowsElement>(new Uri(WindowsApplicationDriverUrl), opt);
            Assert.IsNotNull(session);
        }

        public static void Close()
        {
            if (session != null)
            {
                session.Close();
                session = null;
            }

            if (WinAppDriverProcess != null)
            {
                WinAppDriverProcess.CloseMainWindow();
                WinAppDriverProcess.Close();
            }
        }

    }
}
