﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium.Appium.Windows;
using System;
using System.Threading;

namespace WinAppDriver
{
    public class CalculatorPageObject : Session
    {
        public CalculatorPageObject() { }

        public static WindowsElement Header => session.FindElementByAccessibilityId("Header");

        public static WindowsElement ContentPresenter => session.FindElementByAccessibilityId("ContentPresenter");

        public WindowsElement CalculatorResult => session.FindElementByAccessibilityId("CalculatorResults");

        #region action buttons

        public WindowsElement EqualsButton => session.FindElementByName("Equals");

        public WindowsElement ClearButton => session.FindElementByName("Clear"); 
        
        public WindowsElement PlusButton => session.FindElementByName("Plus");

        public WindowsElement DivideButton => session.FindElementByAccessibilityId("divideButton");
        
        public WindowsElement MultiplyButton => session.FindElementByXPath("//Button[@Name='Multiply by']");
        
        public WindowsElement MinusButton => session.FindElementByXPath("//Button[@AutomationId=\"minusButton\"]");

        #endregion

        #region numberButtons

        public WindowsElement OneButton => session.FindElementByName("One");

        public WindowsElement TwoButton => session.FindElementByName("Two");

        public WindowsElement ThreeButton => session.FindElementByAccessibilityId("num3Button");

        public WindowsElement SevenButton => session.FindElementByName("Seven");

        public WindowsElement EightButton => session.FindElementByName("Eight");

        public WindowsElement NineButton => session.FindElementByXPath("//Button[@Name='Nine']");

        #endregion

        public static WindowsElement IdentifyCalculatorMode()
        {
            try
            {
                return Header;
            }
            catch
            {
                return ContentPresenter;
            }
        }

        public static void SetStandardMode()
        {
            var calculatorMode = IdentifyCalculatorMode();

            if (!calculatorMode.Text.Equals("Standard", StringComparison.OrdinalIgnoreCase))
            {
                session.FindElementByAccessibilityId("TogglePaneButton").Click();
                Thread.Sleep(TimeSpan.FromSeconds(1));

                var splitViewPane = session.FindElementByClassName("SplitViewPane");
                splitViewPane.FindElementByName("Standard Calculator").Click();
                Thread.Sleep(TimeSpan.FromSeconds(1));

                Assert.IsTrue(calculatorMode.Text.Equals("Standard", StringComparison.OrdinalIgnoreCase));
            }
        }

        public string GetCalculatorResultText()
        {
            return CalculatorResult.Text.Replace("Display is", string.Empty).Trim();
        }

        public void ClearResults()
        {
            ClearButton.Click();
            Assert.AreEqual("0", GetCalculatorResultText());
        }
    }
}
